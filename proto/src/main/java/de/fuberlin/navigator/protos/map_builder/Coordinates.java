// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: schema/roadnetwork.proto

package de.fuberlin.navigator.protos.map_builder;

/**
 * <pre>
 * A location is geographical point. The coordinate system used is WGS 84
 * </pre>
 *
 * Protobuf type {@code protos.Coordinates}
 */
public final class Coordinates extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:protos.Coordinates)
    CoordinatesOrBuilder {
private static final long serialVersionUID = 0L;
  // Use Coordinates.newBuilder() to construct.
  private Coordinates(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private Coordinates() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new Coordinates();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return de.fuberlin.navigator.protos.map_builder.RoadNetworkProto.internal_static_protos_Coordinates_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return de.fuberlin.navigator.protos.map_builder.RoadNetworkProto.internal_static_protos_Coordinates_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            de.fuberlin.navigator.protos.map_builder.Coordinates.class, de.fuberlin.navigator.protos.map_builder.Coordinates.Builder.class);
  }

  public static final int LAT_FIELD_NUMBER = 1;
  private float lat_ = 0F;
  /**
   * <pre>
   * lat corresponds to the latitude of the point
   * </pre>
   *
   * <code>float lat = 1;</code>
   * @return The lat.
   */
  @java.lang.Override
  public float getLat() {
    return lat_;
  }

  public static final int LON_FIELD_NUMBER = 2;
  private float lon_ = 0F;
  /**
   * <pre>
   * lon corresponds to the longitude of the point
   * </pre>
   *
   * <code>float lon = 2;</code>
   * @return The lon.
   */
  @java.lang.Override
  public float getLon() {
    return lon_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (java.lang.Float.floatToRawIntBits(lat_) != 0) {
      output.writeFloat(1, lat_);
    }
    if (java.lang.Float.floatToRawIntBits(lon_) != 0) {
      output.writeFloat(2, lon_);
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (java.lang.Float.floatToRawIntBits(lat_) != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeFloatSize(1, lat_);
    }
    if (java.lang.Float.floatToRawIntBits(lon_) != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeFloatSize(2, lon_);
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof de.fuberlin.navigator.protos.map_builder.Coordinates)) {
      return super.equals(obj);
    }
    de.fuberlin.navigator.protos.map_builder.Coordinates other = (de.fuberlin.navigator.protos.map_builder.Coordinates) obj;

    if (java.lang.Float.floatToIntBits(getLat())
        != java.lang.Float.floatToIntBits(
            other.getLat())) return false;
    if (java.lang.Float.floatToIntBits(getLon())
        != java.lang.Float.floatToIntBits(
            other.getLon())) return false;
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + LAT_FIELD_NUMBER;
    hash = (53 * hash) + java.lang.Float.floatToIntBits(
        getLat());
    hash = (37 * hash) + LON_FIELD_NUMBER;
    hash = (53 * hash) + java.lang.Float.floatToIntBits(
        getLon());
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static de.fuberlin.navigator.protos.map_builder.Coordinates parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(de.fuberlin.navigator.protos.map_builder.Coordinates prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * A location is geographical point. The coordinate system used is WGS 84
   * </pre>
   *
   * Protobuf type {@code protos.Coordinates}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:protos.Coordinates)
      de.fuberlin.navigator.protos.map_builder.CoordinatesOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return de.fuberlin.navigator.protos.map_builder.RoadNetworkProto.internal_static_protos_Coordinates_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return de.fuberlin.navigator.protos.map_builder.RoadNetworkProto.internal_static_protos_Coordinates_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              de.fuberlin.navigator.protos.map_builder.Coordinates.class, de.fuberlin.navigator.protos.map_builder.Coordinates.Builder.class);
    }

    // Construct using de.fuberlin.navigator.protos.map_builder.Coordinates.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      lat_ = 0F;
      lon_ = 0F;
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return de.fuberlin.navigator.protos.map_builder.RoadNetworkProto.internal_static_protos_Coordinates_descriptor;
    }

    @java.lang.Override
    public de.fuberlin.navigator.protos.map_builder.Coordinates getDefaultInstanceForType() {
      return de.fuberlin.navigator.protos.map_builder.Coordinates.getDefaultInstance();
    }

    @java.lang.Override
    public de.fuberlin.navigator.protos.map_builder.Coordinates build() {
      de.fuberlin.navigator.protos.map_builder.Coordinates result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public de.fuberlin.navigator.protos.map_builder.Coordinates buildPartial() {
      de.fuberlin.navigator.protos.map_builder.Coordinates result = new de.fuberlin.navigator.protos.map_builder.Coordinates(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(de.fuberlin.navigator.protos.map_builder.Coordinates result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.lat_ = lat_;
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.lon_ = lon_;
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof de.fuberlin.navigator.protos.map_builder.Coordinates) {
        return mergeFrom((de.fuberlin.navigator.protos.map_builder.Coordinates)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(de.fuberlin.navigator.protos.map_builder.Coordinates other) {
      if (other == de.fuberlin.navigator.protos.map_builder.Coordinates.getDefaultInstance()) return this;
      if (other.getLat() != 0F) {
        setLat(other.getLat());
      }
      if (other.getLon() != 0F) {
        setLon(other.getLon());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 13: {
              lat_ = input.readFloat();
              bitField0_ |= 0x00000001;
              break;
            } // case 13
            case 21: {
              lon_ = input.readFloat();
              bitField0_ |= 0x00000002;
              break;
            } // case 21
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private float lat_ ;
    /**
     * <pre>
     * lat corresponds to the latitude of the point
     * </pre>
     *
     * <code>float lat = 1;</code>
     * @return The lat.
     */
    @java.lang.Override
    public float getLat() {
      return lat_;
    }
    /**
     * <pre>
     * lat corresponds to the latitude of the point
     * </pre>
     *
     * <code>float lat = 1;</code>
     * @param value The lat to set.
     * @return This builder for chaining.
     */
    public Builder setLat(float value) {

      lat_ = value;
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * lat corresponds to the latitude of the point
     * </pre>
     *
     * <code>float lat = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearLat() {
      bitField0_ = (bitField0_ & ~0x00000001);
      lat_ = 0F;
      onChanged();
      return this;
    }

    private float lon_ ;
    /**
     * <pre>
     * lon corresponds to the longitude of the point
     * </pre>
     *
     * <code>float lon = 2;</code>
     * @return The lon.
     */
    @java.lang.Override
    public float getLon() {
      return lon_;
    }
    /**
     * <pre>
     * lon corresponds to the longitude of the point
     * </pre>
     *
     * <code>float lon = 2;</code>
     * @param value The lon to set.
     * @return This builder for chaining.
     */
    public Builder setLon(float value) {

      lon_ = value;
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * lon corresponds to the longitude of the point
     * </pre>
     *
     * <code>float lon = 2;</code>
     * @return This builder for chaining.
     */
    public Builder clearLon() {
      bitField0_ = (bitField0_ & ~0x00000002);
      lon_ = 0F;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:protos.Coordinates)
  }

  // @@protoc_insertion_point(class_scope:protos.Coordinates)
  private static final de.fuberlin.navigator.protos.map_builder.Coordinates DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new de.fuberlin.navigator.protos.map_builder.Coordinates();
  }

  public static de.fuberlin.navigator.protos.map_builder.Coordinates getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Coordinates>
      PARSER = new com.google.protobuf.AbstractParser<Coordinates>() {
    @java.lang.Override
    public Coordinates parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<Coordinates> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Coordinates> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public de.fuberlin.navigator.protos.map_builder.Coordinates getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

