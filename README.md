# Protos

## Setup Maven Repo
First you need to set up the repository as a Maven repository. For that you need to create an [Access Token](https://git.imp.fu-berlin.de/swp-datenverwaltung-navigation-2023/protos/-/settings/access_tokens). Select scope `api`, enter role `Developer` and give it a random name. Once you created the token, copy and save it (it is displayed in the top of the page). Once you left that page, you CANNOT read the token anymore. Now go to the `~/.m2` repository on your local machine and open the `settings.xml` file. If you don't have this file, just paste this template into it.
```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
    http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
        <server>
            <id>protos</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Private-Token</name>
                        <value>YOUR_TOKEN</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>
```

If you change the protos, you have to run once more : 

```sh
protoc -I=./proto/src/main/java/de/fuberlin/navigator/protos/map_builder/ --java_out=./proto/src/main/java/ ./proto/src/main/java/de/fuberlin/navigator/protos/map_builder/schema/roadnetwork.proto
```

And 

```sh
protoc -I=./proto/src/main/java/de/fuberlin/navigator/protos/metric_builder/ --java_out=./proto/src/main/java/ ./proto/src/main/java/de/fuberlin/navigator/protos/metric_builder/schema/metric.proto
```


If you already have one, just paste the protos server into it. This should be it. When you now want to use classes of the project inside your code, make sure you include them inside your `pom.xml`. Also, when you change something inside the proto project, make sure to run `mvn clean deploy -s settings.xml` afterwards.
